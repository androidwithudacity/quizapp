package in.srishti.quizapp;

/**
 * Created by Srishti.Gupta on 17-01-2018.
 */

/**
 * Enum used for defining type of question
 */
public enum QuestionType {
    TEXT,
    CHECKBOX,
    RADIO
}