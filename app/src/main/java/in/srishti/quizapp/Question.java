package in.srishti.quizapp;

import java.util.ArrayList;

/**
 * Created by Srishti.Gupta on 16-01-2018.
 */

class Question {
    private int mQuesNo; // index of the question (Question#)
    private QuestionType mType; // type of the question
    private ArrayList<Integer> mOptionIdArrList; // array list of the ID(s) related to the question's option(s) view in XML
    private ArrayList<String> mCorrectAnsArrList; // array list of the correct answers for the specific question

    // constructor
    Question(int quesNo, QuestionType type, ArrayList<Integer> quesArrList, ArrayList<String> correctAnsArrList) {
        this.mQuesNo = quesNo;
        this.mType = type;
        this.mOptionIdArrList = quesArrList;
        this.mCorrectAnsArrList = correctAnsArrList;
    }

    /**
     * Method to get the question type (Getter)
     *
     * @return private field - mType
     */
    QuestionType getType() {
        return this.mType;
    }

    /**
     * Method to get the question IDs array list (Getter)
     *
     * @return private field - mQuesIdArrList
     */
    ArrayList<Integer> getOptionIdList() {
        return this.mOptionIdArrList;
    }

    /**
     * Method to calculate score for a question
     *
     * @param userAnsArrList: list of answers selected by the user
     * @return score for the question
     */
    int calculateScore(ArrayList<String> userAnsArrList) {
        if (userAnsArrList.isEmpty()) { // if the user answers' list is empty, return 0
            return 0;
        } else {
            // if the number of answers in the correctAnsList and number of answers selected by user match, then proceed
            // (especially for question with Checkbox options)
            if (userAnsArrList.size() == this.mCorrectAnsArrList.size()) {
                // if all the values in the correctAnsList matches with the userAnsList, then return a positive score
                if (userAnsArrList.containsAll(this.mCorrectAnsArrList)) {
                    return 1;
                }
            }
        }
        return 0;
    }
}