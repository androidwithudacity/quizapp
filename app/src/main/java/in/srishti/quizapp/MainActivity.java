package in.srishti.quizapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public ArrayList<Question> mQuesList = new ArrayList<>(); // array list containing list of all questions and their fields

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mQuesList = createQuesList(); // list of all questions
    }

    /**
     * Method triggered after submitting answers for the quiz
     *
     * @param view: view on which the event is triggered
     */
    public void submit(View view) {
        int total_score = 0; // total score of the user
        for (Question ques : mQuesList) {
            total_score += this.calculateScoreForQues(ques);
        }
        Toast.makeText(this, "You scored " + total_score + "/10.", Toast.LENGTH_SHORT).show(); // total score shown as toast message to the user
    }

    /**
     * Method to calculate score for a question based on its type
     *
     * @param ques: question instance
     * @return score obtained for the question
     */
    private int calculateScoreForQues(Question ques) {
        if (ques != null && ques.getType() != null) {
            switch (ques.getType()) {
                case TEXT:
                    return this.calculateScoreForQues_Text(ques);
                case CHECKBOX:
                    return this.calculateScoreForQues_Checkbox(ques);
                case RADIO:
                    return this.calculateScoreForQues_Radio(ques);
            }
        }
        return 0;
    }

    /**
     * Method to calculate score for a question for which the option is of type EditText
     *
     * @param textQues: EditText question instance
     * @return score obtained for the question
     */
    private int calculateScoreForQues_Text(Question textQues) {
        ArrayList<String> userAnsArrList = new ArrayList<>();
        for (Integer quesId : textQues.getOptionIdList()) {
            userAnsArrList.add(((EditText) findViewById(quesId)).getText().toString().trim().toLowerCase());
        }
        if (userAnsArrList.get(0).equals("")) { // if the user enters no answer in EditText, the value turns out be empty string ('')
            return 0;
        } else {
            return textQues.calculateScore(userAnsArrList);
        }
    }

    /**
     * Method to calculate score for a question for which the options are of type Checkbox
     *
     * @param checkboxQues: CheckBox question instance
     * @return score obtained for the question
     */
    private int calculateScoreForQues_Checkbox(Question checkboxQues) {
        ArrayList<String> userAnsArrList = new ArrayList<>();
        for (Integer quesId : checkboxQues.getOptionIdList()) {
            CheckBox checkBox = findViewById(quesId);
            if (checkBox.isChecked()) { // add the text value(s) of the checked Checkbox(es) by the user
                userAnsArrList.add(checkBox.getText().toString());
            }
        }
        return checkboxQues.calculateScore(userAnsArrList);
    }

    /**
     * Method to calculate score for a question for which the options are of type RadioButton
     *
     * @param radioQues: RadioButton question instance
     * @return score obtained for the question
     */
    private int calculateScoreForQues_Radio(Question radioQues) {
        ArrayList<String> userAnsArrList = new ArrayList<>();
        for (Integer quesId : radioQues.getOptionIdList()) {
            RadioGroup radioGroup = findViewById(quesId);
            int radioButtonId = radioGroup.getCheckedRadioButtonId(); // find the id of the RadioButton which is checked in the RadioGroup
            if (radioButtonId == -1) {
                return 0;
            } else {
                RadioButton radioButton = radioGroup.findViewById(radioButtonId);
                String userAnswerText = radioButton.getText().toString();
                userAnsArrList.add(userAnswerText); // add the text value of the selected RadioButton by the user
            }
        }
        return radioQues.calculateScore(userAnsArrList);
    }

    /**
     * Method to instantiate and initialize list of all questions
     *
     * @return array list of Question type containing all questions
     */
    ArrayList<Question> createQuesList() {
        ArrayList<Question> quesList = new ArrayList<>();
        quesList.add(
                new Question(
                        1,
                        QuestionType.TEXT,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question1_answer);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add("android asset packaging tool");
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        2,
                        QuestionType.CHECKBOX,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question2_drawable);
                                add(R.id.question2_activity);
                                add(R.id.question2_string);
                                add(R.id.question2_color);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question2_drawable));
                                add(getString(R.string.question2_string));
                                add(getString(R.string.question2_color));
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        3,
                        QuestionType.RADIO,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question3_RadioGroup);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question3_yes));
                            }

                        }
                )
        );
        quesList.add(
                new Question(
                        4,
                        QuestionType.TEXT,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question4_answer);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add("froyo");
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        5,
                        QuestionType.CHECKBOX,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question5_extraMail);
                                add(R.id.question5_extraText);
                                add(R.id.question5_extraStream);
                                add(R.id.question5_extraSub);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question5_extraMail));
                                add(getString(R.string.question5_extraSub));
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        6,
                        QuestionType.RADIO,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question6_RadioGroup);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question6_services));
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        7,
                        QuestionType.TEXT,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question7_answer);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add("android packaging kit");
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        8,
                        QuestionType.RADIO,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question8_RadioGroup);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question8_strings));
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        9,
                        QuestionType.RADIO,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question9_RadioGroup);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question9_linear));
                            }
                        }
                )
        );
        quesList.add(
                new Question(
                        10,
                        QuestionType.RADIO,
                        new ArrayList<Integer>() {
                            {
                                add(R.id.question10_RadioGroup);
                            }
                        },
                        new ArrayList<String>() {
                            {
                                add(getString(R.string.question10_logcat));
                            }
                        }
                )
        );
        return quesList;
    }
}